class Person{
   String name;
   final int age;

   Person(this.age, {this.name});

   @override
   String toString() {
    return "I'm $name and I'm $age";
  }
}

class Car{
  final String name;
  final int age;

  Car({this.name, this.age});

   String toString() {
    return "$name '$age";
  }
}
void main(){
  var people = List<Person>();

  for(int i = 0; i < 10; i++){
    people.add(Person(20 + i, name: "Person ${i+1}"));
  }

  for(var person in people){
    print(person);
  }
  print("\n");
  people.forEach((person)=> person.name = "Unnamed");

  var it = people.iterator;
  while(it.moveNext()){
    print(it.current);
  }
  print("\n");

  var cars = people.map((person) => Car(name: person.name, age: person.age));

  var it2 = cars.iterator;
  do {
    print(it2.current);
  } while (it2.moveNext());
}